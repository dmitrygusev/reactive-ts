 // class 
TS_Hello = function() {
    this.callbacks = {};

	//	observers
	this.obs = {};
	this.obs["c"] = ["b","a"];
	this.obs["b"] = ["a"];
	this.obs["a"] = [];

	this.vals = {};
	// TODO Eval at compile time
	this.vals["a"] = null;
	this.vals["b"] = null;
	this.vals["c"] = 14;
	
	this.funcs = {};
	this.funcs["c"] = function() {
		return this.vals["c"];
	};
	this.funcs["b"] = function() {
		return this.vals["c"] / 2;
	};
	this.funcs["a"] = function() {
		return this.vals["b"] * 1;
	};
}

TS_Hello.prototype.__notify = 
function (name) {
		if (typeof this.callbacks[name]
			 !== "undefined") {
			 this.callbacks[name](this.vals[name]);
		}
	}
	
TS_Hello.prototype.set = 
function(name,val) {

	this.vals[name] = val;
	this.__notify(name);
	for (var o in this.obs[name]) {
		var oName = this.obs[name][o];
		this.vals[oName] =
			this.funcs[oName].bind(this)();
		//	Be reactive
		this.__notify(oName);
	}
}

TS_Hello.prototype.get = 
function(name) {
	return this.vals[name];
}

TS_Hello.prototype.change =
function(name, callback) {
	this.callbacks[name] = callback;
}

var ts = new TS_Hello();

ts.change("a", function(a) {
    console.log("a changed = " + a);
});

ts.set("c",15);

console.log("a = " + ts.get("a"));