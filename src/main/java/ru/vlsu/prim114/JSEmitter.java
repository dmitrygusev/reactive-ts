package ru.vlsu.prim114;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.io.IOUtils;
import org.stringtemplate.v4.ST;

import ru.vlsu.prim114.ReactiveTSParser.BinaryExprContext;
import ru.vlsu.prim114.ReactiveTSParser.ExprContext;
import ru.vlsu.prim114.ReactiveTSParser.LiteralExprContext;
import ru.vlsu.prim114.ReactiveTSParser.SimpleExprContext;

public class JSEmitter
{
	private JSListener listener;
	public JSEmitter(
			JSListener listener)
	{
		this.listener = listener;
	}

	public void emitProgram(Consumer<String> ready)
	{
		try
		{
			String programTemplate =
				IOUtils.toString(
					getClass()
						.getResourceAsStream(
								"program.js.st"));
			
			ST programSt = new ST(programTemplate);
			programSt.add("names",
			listener.getNames().stream()
				.map(name -> new Name()
						.setName(name)
						.setDeps(listener.getDependencies(name))
						.setInitialValue(listener.getInitialValue(name))
						.setJsDefn(emitJSFunction(name, listener.getExpression(name))))
				.collect(Collectors.toList()));
			String rendered = programSt.render();
			ready.accept(rendered);
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
		}
	}

	private String emitJSFunction(
			String name,
			ExprContext expr)
	{
		StringBuilder builder = new StringBuilder();
		builder.append("function() { ");
		if (expr == null)
		{
			builder
				.append("return this.vals[\"")
				.append(name)
				.append("\"];");
			
		}
		else
		{
			BinaryExprContext binaryExpr =
					(BinaryExprContext) expr.getChild(0);
			
			builder
			.append("return ")
			.append(operand(binaryExpr.getChild(0)))
			.append(" ")
			.append(binaryExpr.getChild(1).getText())
			.append(" ")
			.append(operand(binaryExpr.getChild(2)))
			.append(";");
		}
		builder.append(" }");
		return builder.toString();
	}

	private Object operand(ParseTree child)
	{
		if (!(child instanceof SimpleExprContext))
		{
			throw new RuntimeException(
					"Internal compiler error: Grammar changed");
		}
		SimpleExprContext simpleExpr = (SimpleExprContext) child;
		if (simpleExpr.getChild(0) instanceof LiteralExprContext)
		{
			return "this.vals[\"" 
			+ simpleExpr.getChild(0).getChild(0).getText()
			+ "\"]";
		}
		
		return simpleExpr.getChild(0).getChild(0).getText();
	}
	
}
