package ru.vlsu.prim114;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import org.antlr.v4.runtime.tree.ParseTree;

import ru.vlsu.prim114.ReactiveTSParser.ConstExprContext;
import ru.vlsu.prim114.ReactiveTSParser.ExprContext;
import ru.vlsu.prim114.ReactiveTSParser.LiteralExprContext;
import ru.vlsu.prim114.ReactiveTSParser.ProgramContext;
import ru.vlsu.prim114.ReactiveTSParser.StmtContext;

final class JSListener extends ReactiveTSBaseListener
{
	private boolean inStmt;
	private String currentName;

	private Set<String> names = new HashSet<>();
	private Map<String, List<String>> dependencies
		= new HashMap<>();
	private Map<String, String> initialValues
		= new HashMap<>();
	
	private Map<String, ExprContext> expressions
		= new HashMap<>();

	private Consumer<String> ready;
	
	public JSListener(Consumer<String> ready)
	{
		this.ready = ready;
	}
	
	@Override
	public void enterStmt(ReactiveTSParser.StmtContext ctx) {
		String name = ctx.getChild(0).getText();

		if (names.contains(name)) {
			throw new RuntimeException(
					"Error at line " 
					+ ctx.start.getLine() + " position "
					+ ctx.start.getStartIndex()
					+ ": Name " + name +
					" already defined");
		}

		names.add(name);
		
		currentName = name;
		inStmt = true;
		
		String value = tryEvalConstantExpr(ctx);
		
		if (value != null)
		{
			initialValues.put(currentName, value);
		}
		else
		{
			ExprContext expression =
					(ExprContext) ctx.getChild(2);
			
			expressions.put(name, expression);
		}
	}
	
	private static String tryEvalConstantExpr(StmtContext ctx)
	{
		ParseTree parent = ctx.getChild(2);
		
		do
		{
			if (parent.getChildCount() > 1)
			{
				return null;
			}
			if (parent instanceof ConstExprContext)
			{
				return parent.getChild(0).getText();
			}
			parent = parent.getChild(0);
		}
		while (true);
	}

	@Override
	public void exitStmt(StmtContext ctx)
	{
		inStmt = false;
	}
	
	@Override
	public void enterLiteralExpr(
			ReactiveTSParser.LiteralExprContext ctx)
	{
		if (!inStmt)
		{
			return;
		}
		
		String otherName =
				ctx.getChild(0).getText();
		
		List<String> deps =
				dependencies.get(currentName);
		
		if (deps == null)
		{
			deps = new ArrayList<>();
			dependencies.put(currentName, deps);
		}
		
		deps.add(otherName);
	}
	
	@Override
	public void exitProgram(ProgramContext ctx)
	{
		new JSEmitter(this).emitProgram(ready);
	}

	public Set<String> getNames() {
		return names;
	}
	
	public List<String> getDependencies(String name) {
		return dependencies.get(name);
	}
	
	public String getInitialValue(String name)
	{
		return initialValues.get(name);
	}
	
	public ExprContext getExpression(String name) {
		return expressions.get(name);
	}
}