package ru.vlsu.prim114;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;

public class Main
{
	public static void main(String[] args) throws IOException
	{
		Options options = new Options()
		.addOption("i", true, "path to input Reactive-TS file")
		.addOption("o", true, "path to output JavaScript file");
		
		try
		{
			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse(options, args);
			
			if (!cmd.hasOption("i") || !cmd.hasOption("o"))
			{
				printHelp(options);
				System.exit(1);
			}
			
			String output =
					compile(cmd.getOptionValue("i"));
			
			try(FileOutputStream outputFile =
					new FileOutputStream(cmd.getOptionValue("o")))
			{
				IOUtils.write(output, 
						outputFile,
						"UTF-8");
			}
		}
		catch (ParseException e)
		{
			printHelp(options);
		}
	}

	private static void printHelp(Options options)
	{
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( "rtsc.jar", options );
	}

	public static String compile(String fileName) throws IOException {
		ANTLRFileStream antlrFileStream =
				new ANTLRFileStream(fileName);
		
		ReactiveTSLexer lexer = 
				new ReactiveTSLexer(antlrFileStream);
		
		CommonTokenStream tokenStream =
				new CommonTokenStream(lexer);
		
		ReactiveTSParser parser =
				new ReactiveTSParser(tokenStream);
		
		ReactiveTSParser.ProgramContext program =
				parser.program();
		
		AtomicReference<String> ready =
				new AtomicReference<>();
		
		new ParseTreeWalker()
			.walk(new JSListener(output -> ready.set(output)),
					program);
		
		return ready.get();
	}
}
