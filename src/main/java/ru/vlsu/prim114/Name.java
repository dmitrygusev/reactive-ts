package ru.vlsu.prim114;

import java.util.List;

public class Name {
	private String name;
	private List<String> deps;
	private String initialValue;
	private String jsDefn;

	public String getName() {
		return name;
	}

	public Name setName(String name) {
		this.name = name;
		return this;
	}

	public List<String> getDeps() {
		return deps;
	}

	public Name setDeps(List<String> deps) {
		this.deps = deps;
		return this;
	}
	
	public String getInitialValue() {
		return initialValue;
	}
	
	public Name setInitialValue(String initialValue) {
		this.initialValue = initialValue;
		return this;
	}
	
	public String getJsDefn() {
		return jsDefn;
	}
	
	public Name setJsDefn(String jsDefn) {
		this.jsDefn = jsDefn;
		return this;
	}
}
