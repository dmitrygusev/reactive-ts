grammar ReactiveTS;

program: stmt+;

stmt : ID '=' expr ;

expr : unaryExpr | binaryExpr;

literalExpr: ID;
binaryExpr: simpleExpr BINARY_OP simpleExpr;
simpleExpr: constExpr | literalExpr;
unaryExpr: UNARY_OP expr ;
constExpr : NUMBER ;

UNARY_OP: '+'|'-';
BINARY_OP : '+'|'-'|'*'|'/';
NUMBER : [0-9]+ ;
ID     : [a-z]+ ;
WS 		 : [ \t\r\n]+ -> skip ;
