 // class 
TS_Hello = function() {
    this.callbacks = {};

	//	observers
	this.obs = {};
	<names:{name | this.obs["<name.name>"] = [<name.deps:{dep | "<dep>"}>];<\n>}>

	this.vals = {};
	// TODO Eval at compile time
	<names:{name | this.vals["<name.name>"] = <if(!name.initialValue)>null<else><name.initialValue><endif>;<\n>}>
	
	this.funcs = {};
	<names:{name | this.funcs["<name.name>"] = <name.jsDefn>;<\n>}>
}

TS_Hello.prototype.__notify = 
function (name) {
		if (typeof this.callbacks[name]
			 !== "undefined") {
			 this.callbacks[name](this.vals[name]);
		}
	}
	
TS_Hello.prototype.set = 
function(name,val) {

	this.vals[name] = val;
	this.__notify(name);
	for (var o in this.obs[name]) {
		var oName = this.obs[name][o];
		this.vals[oName] =
			this.funcs[oName].bind(this)();
		//	Be reactive
		this.__notify(oName);
	}
}

TS_Hello.prototype.get = 
function(name) {
	return this.vals[name];
}

TS_Hello.prototype.change =
function(name, callback) {
	this.callbacks[name] = callback;
}

/*
var ts = new TS_Hello();

ts.change("a", function(a) {
    console.log("a changed = " + a);
});

ts.set("c",15);

console.log("a = " + ts.get("a"));
*/