package ru.vlsu.prim114;
// Generated from ReactiveTS.g4 by ANTLR 4.5.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ReactiveTSParser}.
 */
public interface ReactiveTSListener extends ParseTreeListener {
	/**d
	 * Enter a parse tree produced by {@link ReactiveTSParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(ReactiveTSParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link ReactiveTSParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(ReactiveTSParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link ReactiveTSParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(ReactiveTSParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link ReactiveTSParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(ReactiveTSParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link ReactiveTSParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(ReactiveTSParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ReactiveTSParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(ReactiveTSParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ReactiveTSParser#literalExpr}.
	 * @param ctx the parse tree
	 */
	void enterLiteralExpr(ReactiveTSParser.LiteralExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ReactiveTSParser#literalExpr}.
	 * @param ctx the parse tree
	 */
	void exitLiteralExpr(ReactiveTSParser.LiteralExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ReactiveTSParser#binaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpr(ReactiveTSParser.BinaryExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ReactiveTSParser#binaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpr(ReactiveTSParser.BinaryExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ReactiveTSParser#simpleExpr}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpr(ReactiveTSParser.SimpleExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ReactiveTSParser#simpleExpr}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpr(ReactiveTSParser.SimpleExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ReactiveTSParser#unaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpr(ReactiveTSParser.UnaryExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ReactiveTSParser#unaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpr(ReactiveTSParser.UnaryExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ReactiveTSParser#constExpr}.
	 * @param ctx the parse tree
	 */
	void enterConstExpr(ReactiveTSParser.ConstExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ReactiveTSParser#constExpr}.
	 * @param ctx the parse tree
	 */
	void exitConstExpr(ReactiveTSParser.ConstExprContext ctx);
}