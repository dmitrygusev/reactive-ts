package ru.vlsu.prim114;
import org.junit.Ignore;
import org.junit.Test;

public class Antlr4 {

	@Test @Ignore
	public void t1_antlrGenerateTS()
	{
		org.antlr.v4
			.Tool
			.main(new String[]{
					"src/main/resources/ReactiveTS.g4",
					"-o",
					"src/generated/java"
					});
	}
	
	@Test// @Ignore
	public void t2_helloTS() throws Exception
	{
		org.antlr.v4.gui.TestRig
		.main(new String[]{
				"ru.vlsu.prim114.ReactiveTS", "program"
				//, "-tree"
				, "-gui"
				, "src/test/resources/hello.ts"
				});
//	Thread.sleep(10000000);
		System.out.println("");
	}
	
	@Test
	public void testParse()
			throws Exception
	{
		String fileName = "src/test/resources/hello.ts";
		
		Main.compile(fileName);
	}
}
